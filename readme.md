VibTodo
--
###### Backend Rest APi for ToDo List application

This is the backend api for ToDo List application, using the most important development 
technologies for Rest API's.

Installation:
--
run mvn clean install -U to update your m2 repository and build the project.

Technologies:
--
* Java 8
* SpringBoot
* JPA
* MySQL
* Swagger/Swagger-UI

Using:
--
When the application is running up, just access http://localhost:8080/swagger-ui to view 
Rest documentation. Swagger will show all the endpoints, request/response examples
and operations available for this application.

Also, you can access the backend in: https://vibtodo.herokuapp.com/swagger-ui.html and teste the endpoints.
This is a free dyno on heroku, so, after some hours, the application will be shutdown.

Development for:
--
Vibbra - www.vibbra.com.br | Arthur Soave - atrpaula@gmail.com

Any questions and comments, just mail me.

