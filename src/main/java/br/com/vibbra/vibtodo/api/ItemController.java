package br.com.vibbra.vibtodo.api;

import br.com.vibbra.vibtodo.domain.service.ItemTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("v1/vibtodo")
public class ItemController implements ItemResource {

    @Autowired
    private ItemTaskService itemTaskService;


    @Override
    @GetMapping(value = "/list/{main-task-id}/item")
    public ResponseEntity getItensMainTask(@PathVariable("main-task-id") Integer mainTaskId) {
        return itemTaskService.getSubTaskByMainTaskId(mainTaskId);
    }

    @Override
    @PostMapping(value = "/task/list/{title}/{description}/{main-task-id}")
    public ResponseEntity addSubTask(@PathVariable String title,
                                     @PathVariable String description,
                                     @PathVariable("main-task-id") Integer mainTaskId) {
        itemTaskService.saveSubTask(title, description, mainTaskId);
        return ResponseEntity
                .created(URI.create(""))
                .body("Ok");
    }

    @Override
    @PutMapping(value = "/task/list/{title}/{description}/{main-task-id}")
    public ResponseEntity updateSubTask(@PathVariable String title,
                                        @PathVariable String description,
                                        @PathVariable("main-task-id") Integer mainTaskId) {
        itemTaskService.saveSubTask(title, description, mainTaskId);
        return ResponseEntity
                .created(URI.create(""))
                .body("Ok");
    }
}
