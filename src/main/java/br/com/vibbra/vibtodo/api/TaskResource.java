package br.com.vibbra.vibtodo.api;

import br.com.vibbra.vibtodo.domain.model.Task;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

@Api("ToDo List - Task Operations")
public interface TaskResource {

    /*
    Must have methods
     */

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get Task", response = Task.class),
            @ApiResponse(code = 403, message = "Forbidden Operation"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Server error")})
    @ApiOperation(value = "get all main tasks")
    ResponseEntity getMainTasks(Integer TaskId);

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Task added ok"),
            @ApiResponse(code = 401, message = "Forbidden Operation"),
            @ApiResponse(code = 403, message = "You shall not pass"),
            @ApiResponse(code = 500, message = "Server error")})
    @ApiOperation(value = "Add a main Task")
    ResponseEntity addMainTask(String title);

}
