package br.com.vibbra.vibtodo.api;

import br.com.vibbra.vibtodo.domain.service.MainTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("v1/vibtodo")
public class TaskController implements TaskResource {

    @Autowired
    private MainTaskService mainTaskService;

    @Override
    @GetMapping(value = "/task/list/{id}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity getMainTasks(@PathVariable("id") Integer idTask) {
        return mainTaskService.getMainTaskById(idTask);
    }

    @Override
    @PostMapping(value = "/task/list/{title}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity addMainTask(@PathVariable("title") String title) {
        mainTaskService.addMainTask(title);
        return ResponseEntity
                .created(URI.create(""))
                .body("Ok");
    }
}
