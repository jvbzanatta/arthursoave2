package br.com.vibbra.vibtodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VibtodoApplication {

    public static void main(String[] args) {
        SpringApplication.run(VibtodoApplication.class, args);
    }

}
