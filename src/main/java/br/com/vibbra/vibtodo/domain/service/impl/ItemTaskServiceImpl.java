package br.com.vibbra.vibtodo.domain.service.impl;

import br.com.vibbra.vibtodo.domain.model.Task;
import br.com.vibbra.vibtodo.domain.repository.TaskRepository;
import br.com.vibbra.vibtodo.domain.service.ItemTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemTaskServiceImpl implements ItemTaskService {

    @Autowired
    private TaskRepository taskRepository;


    @Override
    public void saveSubTask(String title, String description, Integer mainTaskId) {
        Task task = new Task();
        task.settaskName(title);
        task.settaskDescription(description);
        task.setMaintask(false);
        task.setMainTaskid(mainTaskId);
        taskRepository.save(task);
    }

    @Override
    public ResponseEntity<List<Task>> getSubTaskByMainTaskId(Integer mainTaskId) {
        List<Task> tasks = taskRepository.find(mainTaskId);
        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
    }
}
