package br.com.vibbra.vibtodo.domain.model;

import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

@JsonRootName(value = "task")
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Integer taskId;

    @Column(name = "task_title")
    private String taskTitle;

    @Column(name = "task_description")
    private String taskDescription;

    @Column(name = "is_main_task")
    private boolean isMaintask;

    @Column(name = "main_task_id")
    private Integer mainTaskid;

    public Integer getMainTaskid() {
        return mainTaskid;
    }

    public void setMainTaskid(Integer mainTaskid) {
        this.mainTaskid = mainTaskid;
    }

    public Integer gettaskId() {
        return taskId;
    }

    public void settaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String gettaskName() {
        return taskTitle;
    }

    public void settaskName(String taskName) {
        this.taskTitle = taskName;
    }

    public String gettaskDescription() {
        return taskDescription;
    }

    public void settaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public boolean isMaintask() {
        return isMaintask;
    }

    public void setMaintask(boolean maintask) {
        isMaintask = maintask;
    }
}
