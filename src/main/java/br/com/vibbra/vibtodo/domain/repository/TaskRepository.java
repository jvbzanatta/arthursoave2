package br.com.vibbra.vibtodo.domain.repository;

import br.com.vibbra.vibtodo.domain.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    @Query("SELECT t FROM Task t WHERE t.mainTaskid = :mainTaskId")
    List<Task> find(@Param("mainTaskId") Integer mainTaskId);

}
