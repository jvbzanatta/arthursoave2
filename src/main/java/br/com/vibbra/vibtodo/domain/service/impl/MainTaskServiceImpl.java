package br.com.vibbra.vibtodo.domain.service.impl;

import br.com.vibbra.vibtodo.domain.model.Task;
import br.com.vibbra.vibtodo.domain.repository.TaskRepository;
import br.com.vibbra.vibtodo.domain.service.MainTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainTaskServiceImpl implements MainTaskService {

    @Autowired
    private TaskRepository taskRepository;


    @Override
    public void addMainTask(String title) {
        Task task = new Task();
        task.settaskName(title);
        task.setMaintask(true);
        taskRepository.save(task);
    }

    @Override
    public void updateMainTask(Task task) {

    }

    @Override
    public void deleteMainTask(Integer idTask) {

    }

    @Override
    public List<Task> getAllMainTasks() {
        return taskRepository.findAll();
    }

    @Override
    public ResponseEntity<Task> getMainTaskById(Integer idTask) {
        return taskRepository.findById(idTask)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }
}
