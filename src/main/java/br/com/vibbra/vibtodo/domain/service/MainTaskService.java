package br.com.vibbra.vibtodo.domain.service;

import br.com.vibbra.vibtodo.domain.model.Task;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MainTaskService {

    void addMainTask(String title);
    void updateMainTask(Task task);
    void deleteMainTask(Integer idTask);
    List<Task> getAllMainTasks();
    ResponseEntity<Task> getMainTaskById(Integer idTask);
}
