package br.com.vibbra.vibtodo.domain.service;

import br.com.vibbra.vibtodo.domain.model.Task;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ItemTaskService {

    void saveSubTask(String title, String description, Integer mainTaskId);
    ResponseEntity<List<Task>> getSubTaskByMainTaskId(Integer mainTaskId);
}
